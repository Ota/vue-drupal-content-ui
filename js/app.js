/**
 * @file
 * Content UI behaviors.
 */

(function (url, store) {

  'use strict';

  /* global Vue */
  /* global VuePager */
  Vue.component('pager', VuePager);

  Vue.component('table-sort-indicator', Vue.extend({
    template: '#content-ui-table-sort-indicator',
    props: {
      order: null,
      default: false
    },
    computed: {
      sort: function () {
        return this.$route.query.sort;
      },
      show: function () {
        return this.$route.query.order ? this.$route.query.order === this.order : this.default;
      },
      sortClass: function () {
        return 'tablesort tablesort--' + (this.$route.query.sort === 'asc' ? 'desc' : 'asc');
      }
    }
  }));

  Vue.directive('t', function (event) {
    event.innerHTML = Drupal.t(event.innerHTML.trim());
  });

  // Overview page.
  var List = Vue.extend({

    template: '#content-ui-list',

    mounted: function () {
      this.updateData();
    },

    data: function () {
      return {
        content: [],
        loading: false,
        totalPages: 1,
        type: [],
        typeOptions: [],
        status: [],
        statusOptions: [],
        title: [],
      };
    },

    computed: {
      typePath: function () {
        return this.buildPath('type');
      },
      statusPath: function () {
        return this.buildPath('status');
      },
      createdPath: function () {
        return this.buildPath('created');
      },
      updatedPath: function () {
        return this.buildPath('updated');
      },
      userPath: function () {
        return this.buildPath('user');
      }
    },

    watch: {
      $route: 'updateData'
    },

    methods: {

      updateData: function () {
        this.loading = true;
        var that = this;
        store.getRecords(this.$route.query, function (data) {
          that.totalPages = Math.ceil(data.total / 50);
          that.typeOptions = data.typeOptions;
          that.statusOptions = data.statusOptions;
          that.content = data.data;
          that.loading = false;
        });
      },

      filter: function () {
        this.$router.push(this.buildPath());
      },

      buildPath: function (order) {

        var query = {};
        Object.keys(this.$route.query).forEach(param => {
          query[param] = this.$route.query[param]
        })

        // Reset querystring.
        // Set to NULL, UNDEFINED or empty string does not work, so we delete it.
        if (order) {
          query.order = order;
          query.sort = !query.sort || query.sort === 'desc' ? 'asc' : 'desc';
        }
        else {
          delete query.order;
          delete query.sort;
        }

        if (this.type.length) {
          query.type = this.type;
        }
        else {
          delete query.type;
        }

        if (this.status.length) {
          query.status = this.status;
        }
        else {
          delete query.status;
        }

        if (this.title.length) {
          query.title = this.title;
        }
        else {
          delete query.title;
        }

        return {
          path: '/',
          query: query
        };
      },

      reset: function () {
        this.type = [];
        this.status = [];
        this.title = [];
        this.filter();
      },

      activeClass: function (order) {
        return this.$route.query.order === order ? 'is-active' : '';
      }
    }

  });

  /* global VueRouter */
  var router = new VueRouter({
    base: drupalSettings.path.baseUrl + drupalSettings.path.currentPath,
    routes: [
      { path: '/', component: List }
    ]
  })

  var app = new Vue({
    router: router,
    template: '<router-view></router-view>'
  }).$mount('#content-ui-app');

}(Drupal.url, new ContentUiStore()));
