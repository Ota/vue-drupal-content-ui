/**
 * @file
 * Events store service.
 */

window.ContentUiStore = function () {

  'use strict';

  /* global Vue */

  var endPoint = 'api/content/';

  function encodeQueryData(query) {
    var ret = [];
    for (var param in query) {
      ret.push(encodeURIComponent(param) + '=' + encodeURIComponent(query[param]));
    }
    return ret.join('&');
  }

  this.getRecords = function (query, callback) {
    var url = Drupal.url(endPoint + 'content?' + encodeQueryData(query));

    function successCallback(response) {
      response
        .json()
        .then(callback);
    }

    function errorCallback(response) {
      console.error(response);
    }

    Vue.http.get(url).then(successCallback, errorCallback);
  };

};
