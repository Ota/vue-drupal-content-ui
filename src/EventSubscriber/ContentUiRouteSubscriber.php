<?php

namespace Drupal\content_ui\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Content UI route subscriber.
 */
class ContentUiRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Ensure to run after the views route subscriber
    // @see \Drupal\views\EventSubscriber\RouteSubscriber
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -250];
    return $events;
  }


  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $rn => $route) {
      if ($route->getPath() === '/admin/content') {
        $route->setDefault('_controller', '\Drupal\content_ui\Controller\ContentUiController::overview');
      }
    }
  }

}
