<?php

namespace Drupal\content_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Returns responses for DB log UI routes.
 */
class ContentUiController extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbConnection;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Database\Connection $db_connection
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module_handler service.
   */
  public function __construct(DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager, Connection $db_connection, ModuleHandlerInterface $module_handler) {
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->dbConnection = $db_connection;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('module_handler')
    );
  }

  /**
   * Builds the overview page.
   */
  public function overview() {
    $build['content'] = ['#theme' => 'content_ui'];
    $build['#attached']['library'][] = 'content_ui/content_ui';
    return $build;
  }

  /**
   * Returns list of log messages.
   */
  public function content(Request $request) {

    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->dbConnection->select('node_field_data', 'n')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->fields('n')
      ->limit(50);

    // Apply sort parameters.
    $supplied_order = $request->query->get('order');
    $supplied_sort = $request->query->get('sort');

    if ($supplied_order) {
      $allowed_orders = [
        'created' => 'created',
        'updated' => 'changed',
      ];
      $allowed_sorts = [
        'asc' => 'ASC',
        'desc' => 'DESC',
      ];
      if (!isset($allowed_orders[$supplied_order], $allowed_sorts[$supplied_sort])) {
        throw new BadRequestHttpException();
      }

      $order = $allowed_orders[$supplied_order];
      $sort = $allowed_sorts[$supplied_sort];
    }
    else {
      $order = 'changed';
      $sort = 'DESC';
    }

    $query->orderBy($order, $sort);

    $type = $request->query->get('type');
    if ($type) {
      $query->condition('type', explode(',', $type), 'IN');
    }

    $status = $request->query->get('status');
    if (!is_null($status)) {
      $query->condition('status', $status);
    }

    $title = '%' . $request->query->get('title') .'%';
    if (!is_null($title)) {
      $query->condition('title', $title, 'LIKE');
    }

    $total = $query->getCountQuery()->execute()->fetchField();
    $result = $query->execute();

    $data = [];
    foreach ($result as $content) {

      /** @var \Drupal\user\Entity\User $account */
      $account = $this->entityTypeManager->getStorage('user')->load($content->uid);
      $user = [];
      $user['name'] = $account->getDisplayName();
      $user['url'] = $account->isAuthenticated() ? $account->toUrl()->toString() : NULL;

      $node = [];
      $node['url'] = Url::fromRoute('entity.node.canonical', ['node' => $content->nid])->toString();
      $node['edit'] = Url::fromRoute('entity.node.edit_form', ['node' => $content->nid])->toString();
      $node['delete'] = Url::fromRoute('entity.node.delete_form', ['node' => $content->nid])->toString();
      $node['title'] = $content->title;
      $data[] = [
        'node' => $node,
        'id' => $content->nid,
        'type' => $content->type,
        'user' => $user,
        'created' => $this->dateFormatter->format($content->created, 'short'),
        'updated' => $this->dateFormatter->format($content->changed, 'short'),
        'status' => $content->status == 1 ? 'Published' : 'Unpublished',
      ];
    }

    // @todo create filters.
    $filters = content_ui_filters();

    $response = [
      'data' => $data,
      'total' => $total,
      'typeOptions' => isset($filters['type']['options']) ? array_keys($filters['type']['options']) : [],
      'statusOptions' => $filters['status']['options'],
    ];
    return new JsonResponse($response);
  }
}

